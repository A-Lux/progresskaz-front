'use strict'

$(function () {
	// client map
	let clientMapBtn = document.getElementsByClassName('client-map-btn')[0];
	clientMapBtn.addEventListener('click', () => {
		toggleClientMapvisible();
	});

	function isClientMapvisible() {
		return $('.map-block').hasClass('active');
	}

	function toggleClientMapvisible() {
		let mapBlock = $('.map-block');
		mapBlock.toggleClass('active');
		$('#circleslider3').toggleClass('active');
		clientMapBtn.classList.toggle('active');
	}

	// news block
	let newsBlockvisible = false;
	$('.top-news').click(function (event) {
		toggleNewsBlockvisible();
	});

	closeNewsBlock();
	function toggleNewsBlockvisible() {
		$('.news-block').toggleClass('active');
		if (newsBlockvisible) {
			closeNewsBlock();
			$('.client-map-btn').css('opacity', '1');

			if (!isCircleCarouselvisible()) {
				toggleCircleSlidervisible();
			}

			newsBlockvisible = false;
		}
		else {
			visibleNewsBlock();
			$('.client-map-btn').css('opacity', '0');

			if (isClientMapvisible()) {
				toggleClientMapvisible();
			}
			if (isCircleCarouselvisible()) {
				toggleCircleSlidervisible();
			}

			newsBlockvisible = true;
		}
	}

	function visibleNewsBlock() {
		$('.hidden-block.banner-block').css('transform', 'translateX(0%)');
		$('.hidden-block.exectly-news').css('transform', 'translateX(0%)');
	}

	function closeNewsBlock() {
		$('.hidden-block.banner-block').css('transform', 'translateX(300%)');
		$('.hidden-block.exectly-news').css('transform', 'translateX(-300%)');
	}


	// circle carousel
	function isCircleCarouselvisible() {
		return $('#circleslider3').hasClass('active');
	}

	function toggleCircleSlidervisible() {
		$('#circleslider3').toggleClass('active');
	}

	// make curved text
	new CircleType
		(document.getElementById('client-map-btn-text')).radius(350);
	new CircleType
		(document.querySelector('.news-title')).radius(450);



	class CircleSlider {
		constructor(el, dots, dotPositions, visibleDots, queueDots) {
			this.el = document.getElementById(el);
			this.visible = false;
			this.dots = dots;
			this.queueDots = queueDots;
			this.visibleDots = visibleDots;
			this.dotPositions = dotPositions;

			this._initDots();
			this._prepareOffscreenDots();
		}

		_initDots() {
			// circular carousel dot touch
			this.dots.forEach((dot, i) => {
				dot.addEventListener('click', (evt) => {
					if (evt.currentTarget.classList.contains('dot')) {
						this.setActiveSlide(evt.currentTarget);
						evt.stopPropagation();
					}
				});
			});

			this.visibleDots.forEach((dot, i) => {
				dot.style.top = this.dotPositions[i].top + '%';
				dot.style.left = this.dotPositions[i].left + '%';
			});
		}

		nextDot() {
			let dot = queueDots.pop();
			dot.style.display = 'flex';
			dot.style.opacity = '1';
			visibleDots.unshift(dot);

			dot = visibleDots.pop();
			dot.style.opacity = '0';
			setTimeout(() => {
				dot.style.display = 'none';
			}, 500);
			queueDots.unshift(dot);

			if (window.innerWidth > 650) {
				visibleDots[3].classList.remove('active');
				visibleDots[2].classList.add('active');
			} else {
				visibleDots[2].classList.remove('active');
				visibleDots[1].classList.add('active');
			}

			this.placeVisibleDots();

			this._prepareOffscreenDots();
		}

		previousDot() {
			let dot = queueDots.shift();
			dot.style.display = 'flex';
			dot.style.opacity = '1';
			visibleDots.push(dot);

			dot = visibleDots.shift();
			dot.style.opacity = '0';
			setTimeout(() => {
				dot.style.display = 'none';
			}, 500);
			queueDots.push(dot);

			if (window.innerWidth > 650) {
				visibleDots[1].classList.remove('active');
				visibleDots[2].classList.add('active');
			} else {
				visibleDots[0].classList.remove('active');
				visibleDots[1].classList.add('active');
			}

			this.placeVisibleDots();

			this._prepareOffscreenDots();
		}

		placeVisibleDots() {
			visibleDots.forEach((dot, i) => {
				dot.style.top = dotPositions[i].top + '%';
				dot.style.left = dotPositions[i].left + '%';
			});
		}

		isVisible() {
			return this.visible;
		}

		toggleVisible() {
			this.el.classList.toggle('active');
			this.visible = !this.visible;
		}

		_prepareOffscreenDots() {
			this.queueDots[0].style.top = '3%';
			this.queueDots[0].style.left = '68.3%';

			this.queueDots[this.queueDots.length - 1].style.top = '3%';
			this.queueDots[this.queueDots.length - 1].style.left = '30.3%';
		}

		setActiveSlide(slide) {
			let clickedDotIndex = this.visibleDots.findIndex(dot => dot === slide);
			let activeDotIndex = this.visibleDots.findIndex(dot => dot.classList.contains('active'));

			let distance = 0;
			if (clickedDotIndex > activeDotIndex) {
				distance = clickedDotIndex - activeDotIndex
				for (let i = 0; i < distance; i++) {
					if (!isClientMapvisible() && !newsBlockvisible) {
						slider.next();
					}
				}

				// Make dots transition faster
				if (distance > 1) {
					this.visibleDots.forEach((dot, _) => {
						dot.style.transition = 'all 1s';
					});
				}

				let setSlide = () => {
					this.previousDot();
					setTimeout(() => {
						if (--distance > 0) {
							setSlide();
						}
					}, 700);
				}

				setSlide();

				if (distance > 1) {
					this.visibleDots.forEach((dot, _) => {
						dot.style.transition = 'all 1.3s';
					});
				}
			}
			else if (clickedDotIndex < activeDotIndex) {
				distance = activeDotIndex - clickedDotIndex;
				for (let i = 0; i < distance; i++) {
					slider.prev();
				}

				// Make dots transition faster
				if (dispatchEvent > 1) {
					dots.forEach((dot, _) => {
						dot.style.transition = 'all 1s';
					});
				}

				let setSlide = () => {
					this.nextDot();
					setTimeout(() => {
						if (--distance > 0) {
							setSlide();
						}
					}, 700);
				}

				setSlide();

				if (distance > 1) {
					dots.forEach((dot, _) => {
						dot.style.transition = 'all 1.3s';
					});
				}
			}
		}
	}

	// mobile circle slider
	let dots = document.querySelectorAll('#circleslider3 .dot');

	// Set dot positions
	let dotPositions =
		[{ top: 0.3, left: 38.3 },
		{ top: -0.7, left: 49.3 },
		{ top: 0.3, left: 59.1 }];

	if (window.innerWidth > 650) {
		dotPositions.unshift({ top: 3, left: 30.3 });
		dotPositions.push({ top: 3, left: 68.3 });
	}

	// add visible dots
	let numDotsVisible = window.innerWidth > 650 ? 5 : 3;
	let visibleDots = [];
	for (let i = 0; i < numDotsVisible; i++) {
		dots[i].style.display = 'flex';
		dots[i].style.opacity = '1';
		visibleDots.push(dots[i]);
	}

	// add queue dots
	let queueDots = [];
	for (let i = numDotsVisible; i < dots.length; i++) {
		queueDots.push(dots[i]);
	}


	let circleSlider = new CircleSlider('circleslider3', dots, dotPositions, visibleDots, queueDots);
	if (window.innerWidth > 650) {
		dots[1].classList.remove('active');
		dots[2].classList.add('active');
		circleSlider.nextDot();
	}
	// swipe
	let mainContent = document.querySelector('main .content');
	mainContent.addEventListener('touchstart', handleTouchStart, false);
	mainContent.addEventListener('touchmove', handleTouchMove, false);

	var xDown = null;
	var yDown = null;

	function getTouches(evt) {
		return evt.touches ||             // browser API
			evt.originalEvent.touches; // jQuery
	}

	function handleTouchStart(evt) {
		const firstTouch = getTouches(evt)[0];
		xDown = firstTouch.clientX;
		yDown = firstTouch.clientY;
	};

	function handleTouchMove(evt) {
		if (!xDown || !yDown) {
			return;
		}

		var xUp = evt.touches[0].clientX;
		var yUp = evt.touches[0].clientY;

		var notText = !evt.target.classList.contains('text-mobile') &&
			evt.target.tagName != 'SPAN' && evt.target.tagName != 'P';

		var xDiff = xDown - xUp;
		var yDiff = yDown - yUp;

		let dragThreshold = 1;

		if (Math.abs(xDiff) > Math.abs(yDiff)) {/*most significant*/
			if (xDiff > 0 && xDiff > dragThreshold) {
				/* left swipe */
				if (!isClientMapvisible() && !newsBlockvisible) {
					slider.next();
					circleSlider.previousDot();
				}
			} else if (xDiff < 0 && xDiff < dragThreshold) {
				/* right swipe */
				if (!isClientMapvisible() && !newsBlockvisible) {
					slider.prev();
					circleSlider.nextDot();
				}
			}
		} else {
			if (yDiff > 0 && yDiff > dragThreshold) {
				/* up swipe */
				if (!isClientMapvisible() && notText && !newsBlockvisible) {
					slider.prev();
					circleSlider.nextDot();
				}
			} else if (yDiff < 0 && yDiff < dragThreshold) {
				/* down swipe */
				if (!isClientMapvisible() && notText && !newsBlockvisible) {
					slider.next();
					circleSlider.previousDot();
				}
			}
		}
		/* reset values */
		xDown = null;
		yDown = null;
	};


	//SVG Fallback
	if (!Modernizr.svg) {
		$("img[src*='svg']").attr("src", function () {
			return $(this).attr("src").replace(".svg", ".png");
		});
	};


	$("img, a").on("dragstart", function (event) { event.preventDefault(); });

	function Circular(arr, startIntex) {
		this.arr = arr;
		this.currentIndex = startIntex || 0;
	}

	Circular.prototype.next = function () {
		var i = this.currentIndex, arr = this.arr;
		this.currentIndex = i < arr.length - 1 ? i + 1 : 0;
		return this.current();
	}

	Circular.prototype.prev = function () {
		var i = this.currentIndex, arr = this.arr;
		this.currentIndex = i > 0 ? i - 1 : arr.length - 1;
		return this.current();
	}

	Circular.prototype.current = function () {
		return this.arr[this.currentIndex];
	}

	Circular.prototype.iter = function (i) {
		this.currentIndex = i >= 0 && i < this.arr.length ? i : 0;
		return this.currentIndex;
	}

	Circular.prototype.all = function () {
		return this.arr;
	}

	function Slider(data) {
		this.data = data;
	}

	Slider.prototype.all = function () {
		return this.data;
	}

	Slider.prototype.next = function () {

		this.data.all().removeClass("prev-sl quite-prev-sl");
		this.data.all().removeClass("sl-active");
		$(this.data.current()).addClass("prev-sl quite-prev-sl");
		$(this.data.next()).addClass("sl-active");

		return this.data;
	}

	Slider.prototype.prev = function () {

		this.data.all().removeClass("prev-sl quite-prev-sl");
		this.data.all().removeClass("sl-active");
		$(this.data.prev()).addClass("sl-active");
		$(this.data.prev()).addClass("prev-sl quite-prev-sl");
		this.data.next();

		return this.data;
	}

	function Menu(data) {
		this.data = data;
		this.deg = 0;
		this.items = [];
		this.iter_deg = 15;
		this.container = $(".circle");
		this.current_iter = 0;
		this.container.empty();
		let r = $(".circle")[0].getBoundingClientRect().height / 2 - 0.5;

		for (let i = 0; i < 360; i = i + this.iter_deg) {
			var rad = (i * Math.PI) / 180;
			var x = (-r * Math.cos(rad));
			var y = (r * Math.sin(rad));

			if (i == 0) {
				var el = data.current();
			} else {
				var el = data.next();
			}

			var new_el = $("<div class='dot'><div class='dot-name'>" + el.text + "</div> <span class='after'></span></div>");

			this.items.push(new_el);

			this.container.append(new_el);
			new_el.css("position", "absolute");
			new_el.css("top", Math.floor(y + r - new_el.outerHeight() + (new_el.outerHeight() / 2)) + "px");
			new_el.css("left", Math.floor(x + r - new_el.outerWidth() + 5) + "px");
			$(this.items[this.current_iter]).addClass("active");
		}

		for (const key in this.items) {
			this.items[key].click(function (e) {
				var _delta_items = new Circular(Object.keys(menu.items));

				_delta_items.iter(menu.current_iter);

				var _delta_next = 0;
				var _delta_prev = 0;

				while (_delta_items.current() != key) {
					_delta_items.next();
					_delta_next++;
				}

				_delta_items.iter(menu.current_iter);

				while (_delta_items.current() != key) {
					_delta_items.prev();
					_delta_prev--;
				}

				if (Math.abs(_delta_next) > Math.abs(_delta_prev)) {
					var slide_delta = _delta_prev;
				} else {
					var slide_delta = _delta_next;
				}

				for (let i = 0; i < Math.abs(slide_delta); i++) {
					if (slide_delta > 0) {
						if (!isClientMapvisible() && !newsBlockvisible) {
							slider.next();
							menu.next();
						}
					} else if (slide_delta < 0) {
						if (!isClientMapvisible() && !newsBlockvisible) {
							slider.prev();
							menu.prev();
						}
					}
				}
			});
		}
	}

	Menu.prototype.next = function (iter) {

		if (iter) {
			this.deg = (iter * this.iter_deg);
			this.current_iter = iter;
		} else {
			this.deg = (this.deg + this.iter_deg);
			var i = this.current_iter, arr = this.items;
			this.current_iter = i < arr.length - 1 ? i + 1 : 0;
		}

		this.container.css("transform", "rotate(" + this.deg + "deg)");

		for (var key in this.items) {
			this.items[key].css("transform", "rotate(" + (-this.deg) + "deg)");
		}

		$.map(this.items, function (val, i) {
			val.removeClass("active");
		});
		$(this.items[this.current_iter]).addClass("active");

		return this.current_iter;
	}

	Menu.prototype.prev = function () {
		this.deg = (this.deg - this.iter_deg);

		this.container.css("transform", "rotate(" + this.deg + "deg)");

		for (var key in this.items) {
			this.items[key].css("transform", "rotate(" + (-this.deg) + "deg)");
		}

		var i = this.current_iter, arr = this.items;
		this.current_iter = i > 0 ? i - 1 : arr.length - 1;

		$.map(this.items, function (val, i) {
			val.removeClass("active");
		});
		$(this.items[this.current_iter]).addClass("active");

		return this.current_iter;
	}

	var data = new Circular($("[data-slide]"));

	var slider = new Slider(data);

	var menu = new Menu(new Circular([
		{ text: "О КОМПАНИИ" },
		{ text: "ФИЛЬТРАЦИЯ" },
		{ text: "ФЛОТАЦИОННОЕ ОБОРУДОВАНИЕ" },
		{ text: "СУШКА" },
		{ text: "ГАЗООЧИСТКА" },
		{ text: "ПРОИЗВОДСТВО" },
		{ text: "LENSER" },
		{ text: "CLEAR EDGE" },
		{ text: "СЕРВИС" },
		{ text: "ANDRITZ" }
	]));

	$(window).resize(function () {
		setTimeout(function () {
			menu = new Menu(new Circular([
				{ text: "О КОМПАНИИ" },
				{ text: "ФИЛЬТРАЦИЯ" },
				{ text: "ФЛОТАЦИОННОЕ ОБОРУДОВАНИЕ" },
				{ text: "СУШКА" },
				{ text: "ГАЗООЧИСТКА" },
				{ text: "ПРОИЗВОДСТВО" },
				{ text: "LENSER" },
				{ text: "CLEAR EDGE" },
				{ text: "СЕРВИС" },
				{ text: "ANDRITZ" }
			]));
		}, 1500);
	});

	var lock = false;

	$("body").bind('wheel', function (e) {
		if (!lock) {
			lock = true;

			setTimeout(function () {
				lock = false;
			}, 1300);

			var delta = e.originalEvent.deltaY || e.originalEvent.detail || e.originalEvent.wheelDelta;

			if (delta > 0) {
				if (!isClientMapvisible() && !newsBlockvisible) {
					slider.next();
					menu.next();
					circleSlider.nextDot();
				}
			} else {
				if (!isClientMapvisible() && !newsBlockvisible) {
					slider.prev();
					menu.prev();
					circleSlider.previousDot();
				}
			}
		}
	});
});



$('.news-slider').slick({
	prevArrow: $('.next'),
	fade: true,
	nextArrow: $('.prev'),
	dots: false
});

$('.soc-block img').click(function (event) {
	$('.share-btn').addClass('active-btn');
});

$('.soc-block i').click(function (event) {
	var selected = document.querySelector('.social-icon');
	if (selected) {
		if (event.target === selected) {
			$('.share-btn').removeClass('active-btn');
			selected.classList.remove('social-icon');
		}
		else {
			selected.classList.remove('social-icon');
			event.target.classList.add('social-icon');
		}
	}
	else {
		$('.share-btn').addClass('active-btn');
		$(this).addClass('social-icon')
	}
});

$(document).ready(function () {
	let mobileDevice = window.innerWidth < 980;
	if (!mobileDevice) {

		// let cursorWrapper = document.querySelector('.cursor-wrapper');
		// let cursorFollower = document.querySelector('.cursor-follower');
		// let cursor = document.querySelector('.cursor');
		// var timeout;
		// document.addEventListener('mousemove', function (e) {
		// 	clearTimeout(timeout);
		// 	timeout = setTimeout(function () {
		// 		cursorFollower.style.opacity = 0;
		// 	}, 3000);
		// 	cursorFollower.style.opacity = 1;
		// 	cursorFollower.setAttribute('style', `transform: matrix(1, 0, 0, 1, ${(e.pageX)}, ${(e.pageY)});`);
		// 	cursor.setAttribute('style', `transform: matrix(1, 0, 0, 1, ${(e.pageX)}, ${(e.pageY)});`);
		// });

		// let dots = document.querySelectorAll('.circle .dot')
		// dots.forEach(dot => {
		// 	dot.addEventListener('mouseover', function () {
		// 		cursorWrapper.classList.add('active');
		// 	});
		// 	dot.addEventListener('mouseleave', function () {
		// 		cursorWrapper.classList.remove('active');
		// 	});
		// });
		// let others = document.querySelectorAll('.share-btn, .search.transparent-border,.request.transparent-border, main .my-slider .my-slide .dry-pic, main .my-slider .my-slide .more, .copyright-block .soc-block i, .top-news, .slick-arrow, .in-menu ul a')
		// others.forEach(dot => {
		// 	dot.addEventListener('mouseover', function () {
		// 		cursorWrapper.classList.add('active');
		// 	});
		// 	dot.addEventListener('mouseleave', function () {
		// 		cursorWrapper.classList.remove('active');
		// 	});
		// });

		// let menuItems = document.querySelectorAll('.equipment');
		// menuItems.forEach(dot => {
		// 	dot.addEventListener('mouseover', function () {
		// 		cursorWrapper.classList.add('active2');
		// 	});
		// 	dot.addEventListener('mouseleave', function () {
		// 		cursorWrapper.classList.remove('active2');
		// 	});
		// });
	}



	$(function () {
		$(".first:lt(1)").fadeIn();
		setTimeout(function () { $(".first:lt(2)").fadeIn(); }, 2000);
		setTimeout(function () { $(".first:lt(3)").fadeIn(); }, 4000);
		// setTimeout(function(){$(".first:lt(4)").fadeIn();}, 6000);

		setTimeout(function () {
			let divs = document.querySelectorAll(".map-point"),
				len = divs.length,
				index = len - 1;
			let limit = 4;
			const loop = () => {
				$(divs[index]).fadeOut();
				index = ++index % len;
				for (let i = 0; i < limit; i++) {
					let k = (index + i) % len;
					$(divs[k]).fadeIn();
				}
				window.setTimeout(loop, 2000);
			};
			loop();
		}, 6000);

		$(".map-point").hover(
			function () {
				$(this).children(".hiden_text").fadeIn();
			}, function () {
				$(this).children(".hiden_text").fadeOut();
			}
		);

	});
});